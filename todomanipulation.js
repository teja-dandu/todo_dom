// console.log('hello, todo!')
// selecting the input tag by id
const input = document.getElementById('todo-input');
// selecting the button tag by id
// const addButton = document.getElementById('add-button');
// selecting the unorder list of by id only
const todoList = document.getElementById('todo-list');








// this is for adding the text to the todo and if text > 0 then button
//add will be activated otherwise disabled by default
// input.addEventListener('input', onTypeTodo);

// function onTypeTodo() {
//     addButton.disabled = input.value.length === 0;
// }


// "to add button event listener will be click , whenever click on add then that input text will be
// add to the classlist and strored in li that will append to todolist"
input.addEventListener('keypress', onClickAdd);

function onClickAdd(event) {
    if(event.key === 'Enter'){
        const li = createListItem(input.value);
        li.classList.add('task');


        todoList.appendChild(li);
        let tasks = document.querySelector('#todo-list').children;
    // console.log(tasks);
        input.value = '';
    }
    // addButton.disabled = true;

}

function createListItem(name) {
    //creating the li of items to the todoList of unorderlist
    const listItem = document.createElement('li');


    // passing as argument name that text is in h2 heading
    const heading = document.createElement('h2');
    heading.textContent = name;


    const checkbox = document.createElement('input');
    checkbox.type = "checkbox";
    checkbox.classList.add('checkbox');

    checkbox.addEventListener('click', onClickChecked);
    function onClickChecked() {
    // body...
    // console.log(event.target.checked);
        if(this.checked){
            heading.style.color = 'red';
            heading.style.textDecoration = 'line-through';
        }else
        {
            heading.style.color = 'black';
            heading.style.textDecoration = 'none';
        }

    }


    // add to the todolist and then return

    const deleteButton = document.createElement('button');
    deleteButton.textContent = 'X';

    // const markedButton = document.createElement('marked-button');
    // markedButton.textContent = 'X-----X';


    deleteButton.classList.add('delete-button');
    // markedButton.classList.add('marked-button');
    // checkbox.addEventListener('click', onClickChecked);
    deleteButton.addEventListener('click', onClickDelete);


    // listItem.addEventListener('click', onClickMarked);

    listItem.appendChild(checkbox);
    listItem.appendChild(heading);
    listItem.appendChild(deleteButton);
    // listItem.appendChild(activeButton);
    // listItem.appendChild(markedButton);

    return listItem;


}


function onClickDelete() {
    this.parentNode.remove();
}




const markedButton = document.getElementById('marked-button');
markedButton.addEventListener('click', markedCheckList);

// checking for the marked todolistitems
function markedCheckList() {
    let tasks = document.querySelector('#todo-list').children;
    // console.log(tasks.length);
    for(let i=0;i<tasks.length;i++){
        let todoTaskChecked = tasks[i].querySelector('.checkbox');
        if(!todoTaskChecked.checked){
            tasks[i].style.display='none';
        }

    }

}


// adding only active todolistitems and the return
const activeButton = document.getElementById('active-button');
activeButton.addEventListener('click', activeCheckedList);

// checking for the marked todolistitems
function activeCheckedList() {
    let tasks = document.querySelector('#todo-list').children;
    // console.log(tasks.length);
    for(let i=0;i<tasks.length;i++){
        let todoTaskCheckedActive = tasks[i].querySelector('.checkbox');
        if(todoTaskCheckedActive.checked){
            tasks[i].style.display='none';
        }

    }

}


// when the checkedtodolist then cleared that checked todolistitem
const clearedButton = document.getElementById('clear-button');
clearedButton.addEventListener('click', onClickCleared);

// checking for the marked todolistitems
function onClickCleared() {
    let tasks = document.querySelector('#todo-list').children;
    // console.log(tasks.length);
    for(let i=0;i<tasks.length;i++){
        let todoTaskChecked = tasks[i].querySelector('.checkbox');
        if(todoTaskChecked.checked){
            todoTaskChecked.parentNode.remove();
        }

    }

}


// this function render those active button and marked button and return to the listoftodoitems
const allButton = document.getElementById('all-button');
allButton.addEventListener('click', onClickAll);

function onClickAll() {
    let todoList1 = document.querySelector('#todo-list').children;
    for(let i = 0; i < todoList1.length; i++){
        todoList1[i].style.display = 'block';
    }
}
